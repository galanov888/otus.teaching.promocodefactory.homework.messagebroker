﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services
{
    public class SendRabbitMq<T> : ISendRabbitMq<T> where T : class
    {
        private readonly IBus _bus;

        public SendRabbitMq(IBus bus)
        {
            _bus = bus;
        }

        public async Task Send(string uri, T message)
        {
            Uri rabbitUri = new Uri(uri);
            var endPoint = await _bus.GetSendEndpoint(rabbitUri);
            await endPoint.Send(message);
        }
    }
}
