﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services
{
    public interface ISendRabbitMq<T> where T: class
    {
        public Task Send(string uri, T message);
    }
}
