﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.GivingToCustomer.Services.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(PromoCodeDto promoCode, Preference preference, IEnumerable<Customer> customers)
        {

            var promocode = new PromoCode();
            promocode.Id = promoCode.Id;

            promocode.PartnerId = promoCode.PartnerId;
            promocode.Code = promoCode.Code;
            promocode.ServiceInfo = promoCode.ServiceInfo;

            promocode.BeginDate = promoCode.BeginDate;
            promocode.EndDate = promoCode.EndDate;

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            promocode.Customers = new List<PromoCodeCustomer>();

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            };

            return promocode;
        }
    }
}
