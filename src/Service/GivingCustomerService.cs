﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MassTransit;
using Shared.Models;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Linq;
using Otus.Teaching.Pcf.GivingToCustomer.Services.Mappers;

namespace Otus.Teaching.Pcf.GivingToCustomer.Services
{
    public class GivingCustomerService : IConsumer<PromoCodeDto>
    {
        private IRepository<PromoCode> _promoCodesRepository;
        private IRepository<Preference> _preferencesRepository;
        private IRepository<Customer> _customersRepository;

        public GivingCustomerService(IRepository<PromoCode> promoCodesRepository, IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }
        public async Task Consume(ConsumeContext<PromoCodeDto> context)
        {
            var promoCodeDto = context.Message;
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(promoCodeDto.PreferenceId);

            if (preference != null)
            {
                //  Получаем клиентов с этим предпочтением:
                var customers = await _customersRepository
                    .GetWhere(d => d.Preferences.Any(x =>
                        x.Preference.Id == preference.Id));

                PromoCode promoCode = PromoCodeMapper.MapFromModel(promoCodeDto, preference, customers);

                await _promoCodesRepository.AddAsync(promoCode);
            }

        }
    }
}
