﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Shared.Models;

namespace Otus.Teaching.Pcf.Administration.Services
{
    public class AdministrationService : IConsumer<PartnerManagerDto>
    {
        private IRepository<Employee> _employeeRepository;

        public AdministrationService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        public async Task Consume(ConsumeContext<PartnerManagerDto> context)
        {
            var employeeId = context.Message.PartnerManagerId;
            var employee = await _employeeRepository.GetByIdAsync(employeeId);

            if (employee != null)
            {
                employee.AppliedPromocodesCount++;

                await _employeeRepository.UpdateAsync(employee);
            }
        }
    }
}
